#!/bin/bash
set -e

#
# Author: Marko Jovicic <markojovicic@gmail.com>
#
# Taken from: http://thepracticalsysadmin.com/introduction-to-logstashelasticsearchkibana/
# Kibana installation http://www.elasticsearch.org/overview/kibana/installation/
# Logstash init scripts https://gist.github.com/jippi/1065761

apt-get install -y openjdk-7-jre
apt-get install -y curl


# elasticsearch
curl -O https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.2.2.deb
dpkg -i elasticsearch-1.2.2.deb
sh -c 'echo "cluster.name: elasticsearch" >> /etc/elasticsearch/elasticsearch.yml'
sh -c 'echo "node.name: \"logstash test\"" >> /etc/elasticsearch/elasticsearch.yml'
update-rc.d elasticsearch defaults
service elasticsearch restart

# redis
apt-get install -y wget
apt-get install -y make
wget http://download.redis.io/releases/redis-2.8.13.tar.gz
tar xzf redis-2.8.13.tar.gz -C /opt
ln -s /opt/redis-2.8.13 /opt/redis
cd /opt/redis
make
make install

cp src/redis-server /usr/local/bin
cp src/redis-cli /usr/local/bin
mkdir /etc/redis
mkdir /var/redis
cp utils/redis_init_script /etc/init.d/redis_6379

# back to install dir to pick up predefined config
cd - 
cp conf/redis.conf /etc/redis/6379.conf
mkdir /var/redis/6379

sudo update-rc.d redis_6379 defaults
/etc/init.d/redis_6379 start

# kibana
wget https://download.elasticsearch.org/kibana/kibana/kibana-3.1.0.tar.gz
tar xvfz kibana-3.1.0.tar.gz -C /opt
ln -s /opt/kibana-3.1.0 /opt/kibana

# kibana config.js 
sed -i 's@elasticsearch: "http://"+window.location.hostname+":9200"@elasticsearch: "http://192.168.18.230:9200"@g' /opt/kibana/config.js

apt-get install -y nginx

# logstash
curl -O https://download.elasticsearch.org/logstash/logstash/logstash-1.4.2.tar.gz
tar zxvf logstash-1.4.2.tar.gz -C /opt
ln -s /opt/logstash-1.4.2 /opt/logstash

# config
mkdir -p /etc/logstash
mkdir -p /var/log/logstash
mkdir -p /var/run/logstash/syncdb
cp conf/logstash.reader.conf /etc/logstash/reader.conf
cp init/logstash-reader /etc/init.d/
chmod +x /etc/init.d/logstash-reader

update-rc.d logstash-reader defaults
service logstash-reader start

# nginx
mkdir /var/www
ln -s /opt/kibana /var/www/kibana

sed -i 's@root /usr/share/nginx/www;@root /var/www/kibana;@g' /etc/nginx/sites-enabled/default
service nginx restart


